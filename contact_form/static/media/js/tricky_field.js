
$(document).ready(function(){
 /*add asterix for requiered fields*/
 $('.required th').find('label').before('<span class="asterix">* <span/>')

 /*hide the field from human users*/
 $('#id_is_spam, #id_tricky').closest('tr, p').hide();
 $('.at-sign').html('@');
});
