# $Id: models.py ef5633b6df44 2009/09/06 14:08:22 jpartogi $

from django.db import models
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import EmailMessage, BadHeaderError
from django.conf import settings

from smtplib import SMTPException


class Message(models.Model):
    sender_name = models.CharField(max_length=50, verbose_name= _('sender name'))
    sender_email = models.EmailField(verbose_name= _('sender e-mail'))
    subject = models.CharField(_('subject'), max_length=100, blank=True)
    content = models.TextField(verbose_name= _('message'))
    created = models.DateTimeField(auto_now_add=True, verbose_name= _('sent'))
    is_spam = models.BooleanField(_('is spam'), default=False)

    def __unicode__(self):
        return ' | '.join((self.subject, self.sender_name, self.sender_email))

class EmailError(Exception):
    pass

@receiver(post_save, sender=Message)
def send_contact_mail(sender, **kwargs):
    message =  kwargs['instance']
    if message.is_spam:
        return
    subject = message.subject

    # message to the sender
    email = EmailMessage(subject,
                         _("Thank you for contacting us. We'll get back to you shortly."),
                         settings.SERVER_EMAIL,
                         (message.sender_email,),
                         headers = {'Reply-To':
                                    settings.DEFAULT_FROM_EMAIL,})
    try:
        email.send(False)
    except (BadHeaderError, SMTPException), e:
        raise EmailError

    message_content = (_('From:%s')%message.sender_name, message.content)
    # message to the department
    email = EmailMessage(_("Contact form: %s")%subject,
                         '\n\n'.join(message_content),
                         settings.SERVER_EMAIL,
                         (settings.DEFAULT_FROM_EMAIL,),
                         headers = {'Reply-To':
                                    ''.join((message.sender_name, '<', message.sender_email, '>'))})

    try:
        email.send(False)
    except (BadHeaderError, SMTPException), e:
        raise EmailError

