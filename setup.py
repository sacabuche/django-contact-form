from setuptools import setup, find_packages


setup(
    name='django-contact-form',
    version='0.0.1',
    description='Contact Form for Django',
    #long_description=open('README.rst').read(),
    author='Daniel Robles Pichardo',
    author_email='daniel@robles.ws',
    license='BSD',
    #url='https://github.com/sorl/sorl-thumbnail',
    packages=find_packages(),
    package_data = {
        'contact_form':[
            'templates/*/*.html',
            'locale/*/LC_MESSAGES/*',
            'static/*/*/*.js',
            'static/*/*/*.css',
        ]
    },
    platforms='any',
    zip_safe=False,
    classifiers=[
        'Development Status :: 1 - Production/Stable',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Framework :: Django',
    ],
)

